<?php

class UserController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$users=User::all();
		return Response::json($users,200);
	}

	// public function create()
	// {
		
	// }


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$user=new User;
		$user->name=Input::get('name');
		$user->age=Input::get('age');
		$success=$user->save();
	 
		if(!$success)
		{
	       	return Response::json("error saving",500);
		}
	 
	        return Response::json("success",201);
		 // User::create(array(
   //          'name' => Input::get('name'),
   //          'age' => Input::get('age')
   //      ));
    
   //      return Response::json(array('success' => true));
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$user=User::find($id);
		if(is_null($user))
		{
		     return Response::json("not found",404);
		}
	 
			 return Response::json($user,200);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$user=User::find($id);
 
		if(!is_null(Input::get('name')))
		{
			$user->name=Input::get('name');
		}
	 
		if(!is_null(Input::get('age')))
		{
			$user->age=Input::get('age');
		}
	 
		$success=$user->save();
	 
		if(!$success)
		{
			return Response::json("error updating",500);
		}
	 
		return Response::json("success",201);
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$user=User::find($id);
		if(is_null($user))
		{
			return Response::json("not found",404);
		}
	 
		$success=$user->delete();
	 
		if(!$success)
		{
			return Response::json("error deleting",500);
		}
	 
		return Response::json("success",200);
	}


}
