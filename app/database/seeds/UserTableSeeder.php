<?php
 
class UserTableSeeder extends Seeder{
 
	public function run(){
 
		DB::table('users');
 
		User::create(array(
			'name'=>'Agung Setiawan',
			'age'=>'20'
		));
 
		User::create(array(
			'name'=>'Hauril Nisfari',
			'age'=>'21'
		));
 
		User::create(array(
			'name'=>'Akhtar',
			'age'=>'40'
		));
 
	}
}